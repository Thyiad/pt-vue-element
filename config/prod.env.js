'use strict'

let host_env = process.argv[2] || "test";
host_env=JSON.stringify(host_env);

module.exports = {
  NODE_ENV: '"production"',
  HOST_ENV: host_env,
}

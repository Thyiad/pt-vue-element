
const host_dic = {
	mock: "https://easymock.thyiad.top/mock/5fabb662f398e40020f988dd/boilerplate",
	test: 'https://dm-medical-test.zhonganonline.com',
	pre: 'https://dm-medical-uat.zhonganonline.com',
	prd: 'https://dm-medical.zhonganonline.com',
}

let _host = host_dic[process.env.HOST_ENV];
_host= host_dic.mock;

export const HOST= _host;

export const LOGIN=HOST+'/open/login';
export const TABLE_LIST=HOST+'/tableList'
export const GET_USERINFO = HOST+'/getUserInfo'
export const UPLOAD=HOST+'/upload';


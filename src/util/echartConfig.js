let echartConfig = {
	getPieConfg(options){
		const {name, legend, data} = options;
		return {
			title:{
				text: name,
				subtext: undefined,
				x: 'center',
			},
			tooltip : {
				trigger: 'item',
				formatter: "{a} <br/>{b} : {c} ({d}%)"
			},
			legend: {
				orient: 'vertical',
				left: 'left',
				data: legend
			},
			series : [
				{
					name: name,
					type: 'pie',
					radius : '55%',
					center: ['50%', '60%'],
					data:data,
					itemStyle: {
						emphasis: {
							shadowBlur: 10,
							shadowOffsetX: 0,
							shadowColor: 'rgba(0, 0, 0, 0.5)'
						}
					}
				}
			]
		}
	},
	getBarConfig(options){
		const {legend, xAxis, data, colors,showLegend} = options;
		let series = legend.map((item, index)=>{
			return {
				name: item,
				type: 'bar',
				borderRadius: 3,
				barWidth: 10,
				barGap: '40%',
				data: data[index],
				itemStyle: {
					normal: {
						color: colors[index],
					}
				},
			};
		})

		return {
			title: {
				show: false,
			},
			tooltip: {},
			legend: {
				show: showLegend,
				data: legend,
				itemGap: 10,
				icon: 'circle',
				align: 'left',
				right: '0',
				itemWidth: 8,
				itemHeight: 8,
				textStyle:{
					color: 'white',
					fontSize: 9,
				}
			},
			xAxis: {
				type: 'category',
				data: xAxis,
				axisLabel:{
					textStyle: {
						color: "#fff",
						fontSize: 10,
					}
				}
			},
			yAxis: {
				type:'value',
				axisLabel: {
					textStyle:{
						color: '#fff',
						fontSize: 10,
					}
				}
			},
			series: series,
		}
	},
	getLineConfig(options){
		const {name, legend, time, data, colors, showLegend} = options;
		return {
			color: colors,
			title: {
				show: false,
				text: name,
				left: 10,
				textStyle: {
					color: 'white',
					fontSize: 20,
					fontWeight: 'bold'
				},
			},
			tooltip : {
				trigger: 'axis',
				axisPointer: {
					type: 'cross',
					label: {
						backgroundColor: '#6a7985'
					}
				}
			},
			legend: {
				show: showLegend,
				right: 10,
				textStyle: {
					color: '#dfdfdf'
				},
				data: legend ? legend.map(name => ({
					name,
					icon: 'circle'
				})) : []
			},
			grid: {
				left: '3%',
				right: '4%',
				bottom: '3%',
				containLabel: true
			},
			xAxis : [
				{
					type : 'category',
					boundaryGap : false,
					data : time,
					axisLabel: {
						color: '#ccc'
					},
					splitLine: {
						lineStyle: {
							show: true,
							color: '#2a303c'
						}
					}
				}
			],
			yAxis : [
				{
					type : 'value',
					axisLabel: {
						color: '#ccc'
					},
					splitLine: {
						lineStyle: {
							show: true,
							color: '#2a303c'
						}
					}
				}
			],
			series: data ? data.map((item, index) => ({
				name: legend[index],
				type:'line',
				areaStyle: {normal: {
						opacity: 0.2
					}},
				data: item,
				itemStyle:{
					normal:{
						lineStyle:{
							color: colors[index]
						}
					}
				}
			})) : []
		};
	}
}

export default echartConfig;

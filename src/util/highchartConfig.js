let highchartConfig={
	getLineConfig(options){
		const {name, legend, time, data, colors,showLegend,tooltips, xSufix} = options;
		let config = {
			chart:{
				backgroundColor:'transparent',
			},
			credits: {
				enabled:false
			},
			title: {
				text: '',
			},
			subtitle: {
				text: null
			},
			plotOptions:{
				series: {
					label: {
						enabled: false,
					},
				}
			},
			colors:colors,
			xAxis: {
				categories: time,
				labels:{
					formatter() {
						return this.value+(xSufix||'');
					},
					style:{
						color: '#fff',
						fontSize:'10px',
						textOutline:"none"
					}
				}
			},
			yAxis: {
				// tickPixelInterval: 16,
				title: {
					text: null
				},
				labels:{
					style:{
						color: '#fff',
						textOutline:"none"
					}
				}
			},
			legend: {
				enabled: showLegend,
			},
			series: data?data.map((item, index)=>{
				return {
					name: legend[index],
					data: item,
				}
			}):[]
		}
		if(tooltips){
			config.tooltip = {
				// head + 每个 point + footer 拼接成完整的 table
				headerFormat: '<span style="font-size:10px">{point.key}</span>',
				formatter: function(){
					return tooltips[this.x]
				},
				footerFormat: '',
				shared: true,
				useHTML: true
			}
		}
		return config;
	},
	getBarConfig(options){
		const {legend, xAxis, data, colors,showLegend} = options;
		let series = legend.map((item, index)=>{
			return {
				name: item,
				data: data[index],
			};
		})

		return {
			chart: {
				type: 'column',
				backgroundColor:'transparent',
			},
			colors:colors,
			legend:{
				enabled: showLegend,
				align:'right',
				verticalAlign:'top',
				x:0,
				y:0,
				itemStyle:{
					color:'white',
					fontSize: '8px',
					fontWeight: 'normal',
					textOutline:"none"
				},
			},
			credits: {
				enabled:false
			},
			title: {
				text: null
			},
			subtitle: {
				text: null
			},
			xAxis: {
				categories: xAxis,
				crosshair:  true,
				labels:{
					style:{
						color: '#fff',
						fontSize: '10px',
					}
				}
			},
			yAxis: {
				min: 0,
				tickPixelInterval: 16,
				title: {
					text: null
				},
				labels:{
					style:{
						color: '#fff',
						textOutline:"none"
					}
				}
			},
			tooltip: {
				// head + 每个 point + footer 拼接成完整的 table
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					borderWidth: 0
				}
			},
			series: series
		}
	},
}

export default highchartConfig;

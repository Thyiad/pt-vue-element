import {cache} from '../util/tools';
import * as CacheKeys from '../constant/cache-keys';



let _token = '';

let auth = {
	init(){
		_token = cache(CacheKeys.TOKEN);
	},
	getToken() {
		return _token;
	},
	setToken(token) {
		_token = token;
		cache(CacheKeys.TOKEN, token);
	},
	clearToken() {
		_token = '';
		cache(CacheKeys.TOKEN, null)
	}
}

export default auth;
export const noAuthRoutes = ['Login','Register','Forget','NotFound']
export const authLogin = true;

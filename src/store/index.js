import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)

import {expandTree} from '../util/tools'

export default new Vuex.Store({
	state:{
		resourceList: []
	},
	getters:{
		// brandTypesWithoutChildren(state){
		// 	let arr = expandTree(state.brandTypes);
		// 	return arr;
		// },
	},
	mutations: {
		initResourceList(state, data){
			state.resourceList = data;
		}
		// initBrandTypes(state, data){
		// 	state.brandTypes = data;
		// },
	}
})

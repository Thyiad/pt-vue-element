import Vue from 'Vue'

export default {
    // 没有 data，无实例（没有 this 上下文）
    functional: true,
    render: function (h, ctx) {
        const rowData = ctx.props.data;
        const time = new Date().getTime()
        let _type = {};
        if (rowData.beginTime > time) {
            // 未开始
            _type = { type: 'warning', name: '未开始'}
        } else if (rowData.beginTime <= time && time <= rowData.endTime) {
            // 进行中
            _type = { type: 'info', name: '进行中'}
        } else if (time > rowData.endTime ) {
            // 已结束
            _type = { name: '已结束'}
        }
        return h('za-tag', {attrs: {type: _type.type}}, [_type.name]);
    },
    props: {
        data: { type: Object, required: true }
    }
} 
import Vue from 'vue'
export default {
    functional: true,
    render: function (h, ctx) {
            var item = ctx.props.item;
            return h('li', ctx.data, [
                h('div', { attrs: { class: 'complete-name' } }, [item.name]),
                h('span', { attrs: { class: 'complete-text' } }, [item.value])
            ]);
        },
    props: {
        item: { type: Object, required: true }
    }
};
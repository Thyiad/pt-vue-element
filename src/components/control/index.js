import Vue from 'Vue'

const typeEnum = {
	select: "el-select",
	text : "el-input",
	checkbox: 'el-checkbox',
    radio: 'el-radio',
    checkboxes: 'el-checkbox-group',
	"check-button": 'el-button',
}

export default {
    functional: true,
    render: function (h, ctx) {
    	let props = ctx.props,
    		attrs = ctx.data.attrs,
            listeners = ctx.data.on,
            type = typeEnum[props.type],
            componentData = attrs.componentData || {},
            component = {};
    	// if (!type) {
     //        return false;
    	// 	throw new Error(`不支持类型${props.type}控件的动态渲染`);
    	// }
    	if (props.type === 'select' && attrs.options && attrs.options.length > 0) {
            component = h(type, {...ctx.data, on: {change(v) { listeners.change && listeners.change(v)}} }, attrs.options.map(function (item) {
		      	return h('el-option', { props: {value: item.val, label: item.text} });
	    	}));
    	} else if (props.type === 'text' && attrs.prefix) {
            component = h(type, ctx.data, [h('template', { slot: 'append'} , [attrs.prefix] )] );
        } else if (props.type === 'checkboxes' && attrs.options && attrs.options.length > 0) {
            component = h('div', {}, attrs.options.map( item => {
                return h('el-checkbox', { 
                        style: ctx.data.staticStyle,
                        props: {disabled: item.readonly},
                        domProps: {
                            value: attrs.model[item.val]
                        },
                        on: {
                            input: function (event) {
                                attrs.model[item.val] = event;
                            }
                        }
                    }, 
                    item.text
                );
            }));
        } else {
            component = h(type, ctx.data, ctx.children );
        }
     //    console.log(ctx)
     //    if (props.type === 'texts') {
     //        return h('div', {}, attrs.options.map( item => {
     //            return h('el-input', { style: {}, props: {'v-model': attrs.model && attrs.model[item.val]} }, [h('template', { slot: 'append'} , [item.prefix] )] );
     //        }));
     //    }
        if (componentData.tips) {
            component = h('el-tooltip', {
                props: {
                    effect: "dark",
                    content: componentData.tips,
                    placement: 'top'
                }
            }, [component])
        }
    	return component;
    },
    props: {
        type: { 
        	type: String, 
        	required: true 
        },
        text: {
        	type: String,
        	require: true
        }
    }
}
import layout from './layout/index';
import zaPageContent from './layout/page-content.vue';

import zaTable from './table/'
import zaNav from './nav/index'
import zaForm from './form/vue-form'
// import zaActionToolbar from './action-toolbar/index'
// import zaPanel from './panel/index'
// import zaSplit from './split/index'
// import zaControl from './control/index'
// import zaActivityStatus from './activity-status/index'
// import zaTableAction from './table-action/index'
// import zaAutoCompleteItem from './autocomplete/index'
// import zaExport from './export/index'
// import zaSection from './views/section/index'
// import zaSectionControl from './views/section/control'
// import zaSectionPlugin from './views/section/plugin'
// import zaNewCont from './views/content/index'
// import zaShenDialog from './views/content/shenhe'

// import zaBreadCrumb from './breadcrumb/index'
// import zaResContent from './layout/res-content'


const components = {
    layout,
    zaPageContent,
    zaTable,
    zaNav,
	zaForm,
   // zaActionToolbar,
    //zaPanel,
    //zaSplit,
    //zaControl,
    //zaTableAction,
    //zaActivityStatus,
    //zaAutoCompleteItem,
    //zaExport,
    //zaSection,
   // zaSectionControl,
   // zaSectionPlugin,
   // zaBreadCrumb,
   // zaResContent,
    //zaNewCont,
    //zaShenDialog
};

const install = Vue => {
    Object.keys(components).forEach((key) => {
        Vue.component(key, components[key]);
    });
};

export default { install }

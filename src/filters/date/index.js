import moment from 'moment'
export default (value, formatStr = 'YYYY-MM-DD HH:mm:ss') => {
	if (typeof formatStr === "boolean") {
		if (formatStr === true) {
			formatStr = 'YYYY-MM-DD';
		}
	}
	let date = moment(value);
	if (date.isValid()) {
    	return moment(value).format(formatStr);
	} else {
		return "";
	}
};

import date from './date/';
import limit from './limit/';



const filters = {
    date,
    limit
};

const install = Vue => {
    Object.keys(filters).forEach((key) => {
        Vue.filter(key, filters[key]);
    });
};

export default { install };

import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import router from './router/index'
import store from './store/index'
import Components from './components/'
import Filters from './filters/'

import 'font-awesome/css/font-awesome.min.css'
import './assets/styles/common.scss'

import VueQrcode from '@xkeshi/vue-qrcode';
Vue.component(VueQrcode.name, VueQrcode);

Vue.use(ElementUI);
Vue.use(Components);
Vue.use(Filters);


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')


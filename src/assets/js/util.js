import moment from 'moment';
// const GLOBAL = window.global || {venusAddress: '', environment: ''};

// export const env = process.env.NODE_ENV;
// export const VENUS_ADDRESS = GLOBAL.venusAddress.replace(/http\:|https\:/g, '');
// export const ENV = GLOBAL.environment;
// console.log(ENV)

export const dateUtil = {
    format(value, formatStr='YYYY-MM-DD HH:mm:ss') {
        if (formatStr === "date") {
            formatStr = 'YYYY-MM-DD';
        } else if (formatStr === "time") {
            formatStr = 'HH:mm:ss';
        }
        let date = moment(value);
        if (date.isValid()) {
            return date.format(formatStr);
        } else {
            return "";
        }
    },
    create(date) {
        return moment(date);
    },
};
export const formatExportUrl = (prefix, data) => {
    let search = '';
    if (typeof data === 'object') {
        Object.keys(data).forEach( key => {
            if (data[key]) {
                search += `${key}=${data[key]}&`;    
            }
        });
        search = search.slice(0, search.length - 1);
    }
    return prefix + '?' + search;
    
}

const isNumber = (val) => {
    if (val === undefined || val === '') {
        return false;
    }
    return !isNaN(val)
}
// 自定义组件的验证
const validate = {
    // 必填
    required: val => {
        if (!val) {
            return new Error('该项必填');
        }
        return null;
    },
    // 验证正整数
    positiveInteger: (val) => {
        if (!isNumber(val)) {
            return new Error('必须填写数字');
        }
        if (val !== '' && (val <= 0 || parseFloat(val) !== parseInt(val))) {
            return new Error('只能填写正整数');
        }
        return null;
    },
    // 整数
    integer: (val) => {
        if (!isNumber(val)) {
            return new Error('必须填写数字');
        }
        if ( parseFloat(val) !== parseInt(val)) {
            return new Error('只能填写整数');
        }
        return null;
    },
    // 数字
    number: (val) => {
        if (!isNumber(val)) {
            return new Error('必须填写数字');
        }
        return null;
    },
    money: val => {
        if (/^\d+\.?\d{0,2}$/.test(val)) {
            return null;
        } else {
            return new Error('必须是金额类型');
        }
    },
    // 正数
    positive: (val) => {
        if (!isNumber(val)) {
            return new Error('必须填写数字');
        }
        if (val <= 0) {
            return new Error('只能填写正数');
        }
        return null;
    },
    // 字符串
    char: (val) => {
        if (!val) {
            return new Error('必须填写');
        }
        return null;
    },
    min: (val, min) => {
        if (val < min) {
            return new Error('最小值为' + min);    
        }
        return null;
    },
    max: (val, max) => {
        if (val > max) {
            return new Error('最大值为' + max);    
        }
        return null;    
    }
}
export const controlValidate = ( options ) => {
    return (rule, value, callback) => {
        let msg = '';
        if (options.textType) {
            if (validate[options.textType]) {
                msg = validate[options.textType](value);
                if (msg) {
                    return callback(msg);
                }
            }
        }
        if (options.required === true) {
            msg = validate.required(value);
            if (msg) {
                return callback(msg);
            }
        }
        // 验证min, max
        if (options.min || options.min === 0) {
            msg = validate.min(value, options.min);
            if (msg) {
                return callback(msg);
            }
        }
        if (options.max || options.max === 0) {
            msg = validate.max(value, options.max);
            if (msg) {
                return callback(msg);
            }
        }
        return callback();
    };
};

export const getOuterHeight = (element, hasMargin) => {
    let height = element.offsetHeight,
        style = getComputedStyle(element, null);
    if (hasMargin) {
        return height + parseInt(style.marginTop) + parseInt(style.marginBottom);
    } else {
        return height;
    }
};

export const getOuterWidth = (element, hasMargin) => {
    let width = element.offsetWidth,
        style = getComputedStyle(element, null);
    if (hasMargin) {
        return width + parseInt(style.marginLeft) + parseInt(style.marginRight);
    } else {
        return width;
    }
};

export const loop = (data, id, callback, name = "key") => {
    data.forEach((item, index, arr) => {
        if (item[name] === id) {
            return callback(item, index, arr);
        } else if (id === null) {
            callback(item, index, arr);
        }
        if (item.slots) {
            return loop(item.slots, id, callback, name);
        }
    });
};

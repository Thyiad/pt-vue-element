let menus = [
	{
		text: '首页',
		iconCls: 'el-icon-tickets',
		modulepath: '/dashboard/index',
	},
	{
		text: '表格',
		iconCls: 'el-icon-tickets',
		modulepath: '/table-list/index',
	},
	// {
	// 	text: '爬虫数据',
	// 	iconCls: 'el-icon-tickets',
	// 	modulepath: '/spider/index',
	// 	children: [
	// 		{
	// 			text: '疾病库',
	// 			iconCls: 'el-icon-tickets',
	// 			modulepath: '/spider/index',
	// 		}
	// 	]
	// },
];

export default menus;

export function getFormBasicConfig(config){
	let {value, organList, sexList, marryList, levelList} = config;

	let validateAge = (rule, value, callback, source, options)=>{
		if(value==null || value===''){
			callback()
			return
		}
		if(!/^\d+$/.test(value)){
			callback(new Error("请填写合法的数字"))
			return;
		}
		let targetValue = parseInt(value);
		if(targetValue<=0){
			callback(new Error('年龄不能小于0'))
			return;
		}
		callback();
	}

	let rules = {
		disName: [
			{required: true, message: '疾病名称不能为空', trigger: 'blur'}
		],
	}

	let targetConfig = {
		cols: 3,
		'label-width': '135px',
		'input-enter-submit': false,
		values: value,
		rules: rules,
		'default-field-col': 1,
		fields: [
			// 疾病名称
			{
				type: 'input',
				prop: 'disName',
				label: '疾病名称',
				options: {
					placeholder: '疾病名称',
				}
			},
			// 特定性别
			{
				type: 'select',
				prop: 'specialSexId',
				label: '特定性别',
				options: {
					options: sexList,
				}
			},
			// 疾病简介
			{
				type: 'richtext',
				prop: 'summary',
				label: '疾病简介',
				options: {
					type: 'textarea',
					placeholder: '输入疾病简介'
				},
				col: 3,
			},
		],
		buttons: [],
	}

	return targetConfig;
}

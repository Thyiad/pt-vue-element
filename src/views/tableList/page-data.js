/**
 * Created by Thyiad on 2018/9/13 0013.
 */
export const searchConfig = {
	cols: 2,
	'label-width': '130px',
	'input-enter-submit': true,
	fields:[
		{
			type: 'input',
			prop: 'keywords',
			label: '关键字',
		},
	],
	'btns-position':'',
	'btns-style':{
		'margin': 0,
		'justify-content': 'flex-start'
	},
	buttons:[
		{
			text:'查询',
			icon:'el-icon-search',
			cls:'search-btn',
			event: 'submit',
		},
	]
}
